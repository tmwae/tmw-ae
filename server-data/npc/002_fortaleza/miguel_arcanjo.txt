



002,100,100,0	script	Miguel Arcanjo	NPC302,{
    if (QUEST_quintus == 3) goto L_Fala;
    if (QUEST_quintus == 4) goto L_Cont;
    if (QUEST_quintus == 5) goto L_dar_premio;
    if (QUEST_quintus >= 6) goto L_Fim;

L_Nao2:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Olá!\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Olá...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Quem é você?", L_Info,
        "O que está fazendo aqui?", L_Info;

L_Info:
    mes "[Miguel Arcanjo]";
    mes "\"Isto não é de sua conta! Vá embora e me deixe em paz!\"";
    close;

L_Fala:
    mes "[" + strcharinfo(0) + "]";
    mes "\"Miguel Arcanjo?\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Você me conhece?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Conheci Quintus, e ele me contou sua historia...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Sim, ora bolas, por onde anda esse rapaz?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Está de guarda, parecendo um sentinela...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Ele deveria me proteger, mas em vez disso fica construindo coisas, espero que não tenha conseguido mais madeira, se não vai passar um bom tempo lá...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Hmm, madeira é?\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Sim, por que tem algo de estranho nisso?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Não, nada não...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Sim, e o que trás você aqui?\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "Vim saber sobre a sua Espada, ela é muito poderosa não é mesmo?", L_poder,
        "Nada de mais, só queria conhecer a sua pessoa...", L_Saia;

L_Saia:
    mes "[Miguel Arcanjo]";
    mes "\"Pois bem, prazer em conhecer, agora pode ir embora daqui!\"";
    close;

L_poder:
    mes "[Miguel Arcanjo]";
    mes "\"Não tem muita coisa de mais, só quebra algumas coisas mais resistentes...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Ela é capaz de matar Anjos e Demônios...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Esse Quintus e sua boca grande...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Então, eu preciso dessa espada, e gostaria de ter ela...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Então meu caro, vai ficar querendo...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Mas, Arcanjo, sou um guerreiro poderoso e posso destruir os monstros que atormentam essa cidade!\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Está tentando me enganar, mas já que é tão forte pode me ajudar a refazer minhas asas, dai eu vejo o que posso fazer por você...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Só isso? Está feito...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Pois bem, então pegue esta lista aqui e traga estas coisas...\"";
    next;
    mes "O arcanjo pega um pedaço de papel e anota algumas coisas.";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Nesse papel tem o que eu vou precisar... Seria bom você anotar em algo que sempre ande contigo, ou guardar bem este papel...\"";
    next;
    mes "Você olha o papel e nele está escrito:";
    mes "";
    mes "[Lista de Materiais]";
    mes "* 100 Tocos de madeira,";
    mes "* 50 Pós de enxofre,";
    mes "* 50 Peles de chacal,";
    mes "* 30 Peles de serrastes,";
    mes "* 30 Teias de aranha,";
    mes "* 20 Asas de fada,";
    mes "* 10 Minérios Dourados, ";
    mes "* 5 Runas antigas,";
    mes "* 5 Poções da Morte,";
    mes "* 5 Escamas de Dragão,";
    mes "* 1 Elixir";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Isso não é justo, vou levar uma eternidade pra conseguir tudo isso...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Se fosse fácil, eu já estaria com minhas asas. Devia ter perguntado antes de aceitar, agora vá e traga tudo!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Está bem, trarei...\"";
    QUEST_quintus = 4;
    close;

L_Cont:
    mes "[Miguel Arcanjo]";
    mes "Então, trouxe tudo?";
    next;
    menu
        "Sim, demorou, mas consegui...?", _sim,
        "Deixa-me ver a lista novamente...", _lista,
        "Ainda não...", _ainda;
    close;

L_ainda:
    mes "[Miguel Arcanjo]";
    mes "\"Ainda não...\"; \"Então não perca tempo, vá e consiga tudo...\"";
    close;

L_Lista2:
    mes "[Lista de Materiais]";
    mes "* 100 Tocos de madeira,";
    mes "* 50 Pós de enxofre,";
    mes "* 50 Peles de chacal,";
    mes "* 30 Peles de serrastes,";
    mes "* 30 Teias de aranha,";
    mes "* 20 Asas de fada,";
    mes "* 10 Minérios Dourados, ";
    mes "* 5 Runas antigas,";
    mes "* 5 Poções da Morte,";
    mes "* 5 Escamas de Dragão,";
    mes "* 1 Elixir";
    close;

L_Sim:
    if (countitem("TocoDeMadeira") < 100 ||
        countitem("PoDeEnxofre") < 50 ||
        countitem(xxx) < 50 ||  // id peles de chacal
        countitem(xxx) < 30 ||  // peles de serrastes
        countitem("TeiaDeAranha") < 30 ||
        countitem("AsasDeFada") < 20 ||
        countitem(xxx) < 10 ||  // minerios dourados
        countitem(xxx) < 5 ||   // runas
        countitem("PocaoDaMorte") < 5 ||
        countitem("EscamasDeDragao") < 5 ||
        countitem(xxx) < 1      // elixir
    ) goto L_sem_itens;

    delitem "TocoDeMadeira", 100;
    delitem "PoDeEnxofre", 50;
    delitem xxx, 50;
    delitem xxx, 30;
    delitem "TeiaDeAranha", 30;
    delitem "AsasDeFada", 20;
    delitem xxx, 10;
    delitem xxx, 5;
    delitem "PocaoDaMorte", 5;
    delitem "EscamasDeDragao", 5;
    delitem xxx, 1;

    QUEST_quintus = 5;

L_dar_premio:
    mes "[Miguel Arcanjo]";
    mes "\"Deixe-me ver\"";
    next;
    mes "O Arcanjo começa a cortar as peles, as madeiras e se volta para você";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Pelo visto isso vai servir...\"";
    mes "[" + strcharinfo(0) + "]";
    mes "\"Foi trabalhoso...\"";
    next;
    mes "[Miguel Arcanjo]";
    mes "\"Está bem, muito obrigado pela ajuda, até mais...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Nada disso, cumpra sua parte no combinado, quero minha espada!\"";
    next;
    mes "[Miguel Arcanjo]";
    //<< Aqui cabe um teste de força. A espada é capaz de matá-lo se ele não tiver força para resistir ao seu poder.
    mes "\"Sim, é mesmo, já estava me esquecendo , tome sua espada...\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    mes "\"Sei, se esquecendo...\"";
    getinventorylist;
    if (@inventorylist_count == 100) goto L_Cheio;
    getitem "EspadaMiguelArcanjo", 1;
    QUEST_quintus = 6;
    mes "[Miguel Arcanjo]";
    mes "\"Até mais, e mais uma vez, obrigado pela ajuda...\"";
    close;

L_sem_itens:
    mes "[Miguel Arcanjo]";
    mes "\"Não tem tudo, vá e consiga o tudo...\"";
    close;

L_Fim:
    mes "[Miguel Arcanjo]";
    mes "\"Agora só restaurar minhas asas ... uhuuu\"";
    close;

L_Cheio:
    mes "[Miguel Arcanjo]";
    mes "\"Você está carregando coisas demais... livre-se de algumas delas e volte a falar comigo.\"";
    close;
}
