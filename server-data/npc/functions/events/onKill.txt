

-	script	#onKill	NPC32767,{
    end;

OnPCKillEvent:
    Zeny = (Zeny + (BaseLevel*100));
    message strcharinfo(0), l("* You've killed another player! You'll receive money for this.");

    @n = rand(3);
    if (@n == 0) message strcharinfo(0), l("Die!");
    if (@n == 1) message strcharinfo(0), l("Mwuhahahaha!");
    if (@n == 2) message strcharinfo(0), l("You have no chances against me.");
    end;
}
