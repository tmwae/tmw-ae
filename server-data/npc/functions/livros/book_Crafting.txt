

function	script	openCraftingBook	{
    mes "[LIVRO DE ARTESANATO]";
    mes "";
    mes "Os artesanatos ensinados neste livro podem ser feitos por todos os cidadãos do mundo. "+
    "Desde o mais fraco até o mais forte. Pois qualquer uma pessoa pode usar suas criatividade "+
    " e suas mãos para combinar objetos inventando novas ferramentas uteis.";
    mes "";
    mes " → #poisonarrow";
    mes "    * Cria 200 [" + getitemlink("FlechaEnvenenada") + "].";
    mes "   Necessita de:";
    mes "    * 1 Frasco de [" + getitemlink("VenenoDeNinfas") + "];"; // ← Pode ser conseguido por quest ou por habilidade de ninja
    mes "    * 200 [" + getitemlink("Flecha") + "]."; // ← Pode ser comprada em loja de arqueirismo ou criada por ninja
    mes "";
    mes " → #poisoniron";
    mes "    * Cria 200 [" + getitemlink("FlechaFerroEnvenenada") + "].";
    mes "   Necessita de:";
    mes "    * 1 Frasco de [" + getitemlink("VenenoDeNinfas") + "];"; // ← Pode ser conseguido por quest ou por habilidade de ninja
    mes "    * 200 [" + getitemlink("FlechaDeFerro") + "]."; // ← Pode ser comprada em loja de arqueirismo ou criada por ninja
    mes "";
    return;
}
