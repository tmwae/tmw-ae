


function	script	bilheteVermeGuloso	{
    if (@bilheteVerme > 1) goto L_Return;

    getitem "BilheteMagico", 1;
    @bilheteVerme = 2;
    message strcharinfo(0), "Consegui um Bilhete Mágico!";
    return;

L_Return:
    return;
}



function	script	sumonaAbelhas	{
    @n = 4 - mobcount(@mapa$, @label$);
    if (@n < 1) goto L_Return;
    //if (@n > 5) set @n, 5;
    areamonster @mapa$, @x-2, @y-2, @x+2, @y+2, "", 1049, @n, @label$;
    return;

L_Return:
    return;
}
