function	script	useShockSweet	{
    if (rand(5))
        goto L_change;
    message strcharinfo(0), "Eca, este teve gosto de cera!";
    heal -20 -(Hp >> 2), 0;
    misceffect 15, strcharinfo(0);
    return;

L_change:
    setlook LOOK_HAIR_COLOR, HC_WHITE;
    message strcharinfo(0), "Oh, isto tem um gosto forte!";
    sc_start SC_POISON, 1, 20;
    return;
}
