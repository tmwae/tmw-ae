function	script	useCristalWarp	{
    if (isin("005", 38, 114, 41, 116) == 0 &&
        isin("009-3", 143, 44, 146, 46) == 0 &&
        isin("halicarnazo", 112, 88, 115, 90) == 0 &&
        isin("froz", 154, 79, 157, 81) == 0 &&    
        isin("011", 100, 114, 103, 115) == 0 && 
        isin("aeros", 76, 25, 77, 26) == 0
    ) goto L_Falha;
    if (@Lugar$ == "bhramir") goto L_ParaBhramir;
    if (@Lugar$ == "lilit") goto L_ParaLilit;
    if (@Lugar$ == "aeros") goto L_ParaAeros;
    if (@Lugar$ == "halicarnazo") goto L_ParaHalicarnazo;
    if (@Lugar$ == "froz") goto L_ParaFroz;
    if (@Lugar$ == "hades") goto L_ParaHades;
    return;

L_ParaBhramir:
    warp "005", 40, 115;
    getitem "CristalWarpDesenerg", 1;
    return;

L_ParaLilit:
    warp "009-3", 145, 45;
    getitem "CristalWarpDesenerg", 1;
    return;

L_ParaHalicarnazo:
    warp "halicarnazo", 114, 89;
    getitem "CristalWarpDesenerg", 1;
    return;

L_ParaFroz:
    warp "froz", 156, 80;
    getitem "CristalWarpDesenerg", 1;
    return;

L_ParaHades:
    warp "011", 101, 115;
    getitem "CristalWarpDesenerg", 1;
    return;

L_ParaAeros:
    warp "aeros", 76, 26;
    getitem "CristalWarpDesenerg", 1;
    return;

L_Falha:
    message strcharinfo(0), "Preciso estar perto o bastante de um portal para usar este cristal.";
    if (@Lugar$ == "bhramir") getitem "CristalWarpDeMadeira", 1;
    if (@Lugar$ == "lilit") getitem "CristalWarpDeFolha", 1;
    if (@Lugar$ == "aeros") getitem "CristalWarpDeNuvem", 1;
    if (@Lugar$ == "halicarnazo") getitem "CristalWarpDeAreia", 1;
    if (@Lugar$ == "froz") getitem "CristalWarpGelo", 1;
    if (@Lugar$ == "hades") getitem "CristalWarpDeSangue", 1;
    return;
}
