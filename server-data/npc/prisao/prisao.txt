



prisao,22,22,0	script	Delegado Jenésio	NPC150,{
    callfunc "GameRules";
    close;
}

prisao,38,25,0	script	Guarda Clóvis	NPC307,{
    if (prisEst == 1) goto L_foiPreso;
    if (prisEst == 2) goto L_aguarde;
    if (prisEst == 3) goto L_deOlho;

    mes "[Guarda Clóvis]";
    mes "\"Você sim é um cidadão exemplar! Nunca foi preso!";
    mes "Ao contrário destes sujeitos de ficha suja.\"";
    npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' é um cidadão exemplar! Possui a ficha limpa!";
    close;

L_foiPreso:
    mes "[Guarda Clóvis]";
    mes "\"Outro que não respeita ou não conhece as Leis deste Mundo!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Fale como Delegado que ele te dirá quais são estas Leis!\"";
    next;
    mes "[" + strcharinfo(0) + "]";
    menu
        "=S Como vim parar aqui?", L_Porque,
        "=o Sou inocente... me solte!", L_inocente;

L_Porque:
    mes "[Guarda Clóvis]";
    mes "\"É uma boa pergunta!\"";
    next;
    goto L_Explicacao;

L_inocente:
    mes "[Guarda Clóvis]";
    mes "\"Isso é o que todos dizem!\"";
    next;
    goto L_Explicacao;

L_Explicacao:
    mes "[Guarda Clóvis]";
    mes "\"Você foi preso porque descumpriu umas das leis do reino.";
    mes "Por isso terá que pagar pena na prisão!\"";
    $prisTime[prisNum] = 0;
    prisEst = 2;
    close;

L_aguarde:
    if ($prisTime[prisNum] >= prisPena) goto L_cumpriuPena;

    mes "[Guarda Clóvis]";
    @n = prisPena - $prisTime[prisNum];
    if (@n == 1) mes "\"Sua pena está quase terminando. Será libertado em 1 minuto!\"";
    if (@n > 1)  mes "\"Sua pena ainda não terminou. Ainda faltam " + @n + " minutos.\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Enquanto isso informe-se sobre as Leis deste Mundo com o Delegado!\"";
    close;

L_cumpriuPena:
    mes "[Guarda Clóvis]";
    mes "\"Você cumpriu toda a pena!";
    mes "Mas lembre-se que se tornar a cometer crimes contra o reino será enviado para prisão outra vez!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Para não ser preso novamente informe-se sobre as Leis deste Mundo com o Delegado!\"";
    next;
    mes "[Guarda Clóvis]";
    mes "\"Agora você está livre!";
    mes "Faça bom uso de sua liberdade.\"";
    @x = rand(11);
    warp "prisao", 25+@x, 27;
    //savepoint "prisao", 25+@x, 27; // Salvar na prisao é errado... calsa brecha.
    savepoint "005", 132, 82;
    callfunc "delPris";
    close;

L_deOlho:
    mes "[Guarda Clóvis]";
    mes "\"Agora você está fichado. Melhor não cometer mais nenhum crime!\"";
    if (prisQtd == 1) npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' foi fichado uma vez!";
    if (prisQtd > 1) npctalk strnpcinfo(0), "'" + strcharinfo(0) + "' foi fichado " + prisQtd + " vezes!";
    close;




}


function	script	addPris	{
    if (prisEst != 0) goto L_jaFoiPreso;

    $numPris = $numPris + 1;
    prisNum = $numPris;
    $prisPlay[prisNum] = getcharid(3);
    $prisTime[prisNum] = -1;
    prisPena = 15;
    //resetlvl 4;        // Tirava os pontos de status
    //set SkillPoint, 0; // Zerava a skill Basic
    addtoskill SKILL_TRADE, 0; // Reseta a skill de trade.
    prisQtd = 1;
    prisEst = 1;
    return;

L_jaFoiPreso:
    $prisTime[prisNum] = -1;
    prisPena = 15;
    prisQtd = prisQtd + 1;
    //resetlvl 4;        // Tirava os pontos de status
    //set SkillPoint, 0; // Zerava a skill Basic
    addtoskill SKILL_TRADE, 0; // Reseta a skill de trade.
    prisEst = 1;
    return;
}

function	script	delPris	{
    if (prisEst == 0 || prisEst == 3) goto L_Return;

    if (prisQtd > 0) set prisQtd, prisQtd - 1;
    $prisTime[prisNum] = -1;
    prisEst = 3;
    return;

L_Return:
    return;
}
