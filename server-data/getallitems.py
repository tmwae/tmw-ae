#import <script.h>

f=open("db/re/item_db.conf", "r")
w=open("WEAPONS.c", "w")
a=open("ARMOR.c", "w")
e=open("ELMS.csv", "w")

buff=[]
output=[]

save=False

def save_wpn(buff):
        output=[]
        #print str(buff)
        #print "--------------"
        for line in buff:
            if "Id: " in line:
                #print line
                output.append(line.replace("Id: ", " ").replace(" ", "").replace("\n", ""))
            if " Name: " in line:
                #print line
                output.append(line.replace("Name: ", "*").replace(" ", "").replace("\n", ""))
            if "Atk: " in line:
                #print line
                output.append(line.replace("Atk: ", "+").replace(" ", "").replace("\n", ""))
            if "EquipLv: " in line:
                #print line
                output.append(line.replace(" ", "").replace("\n", "").replace("EquipLv:", "Lvl "))
        #print "--------------"

        try:
            #print output[0] + " (" + output[3] + ", " + output[2] + ") " + output[1]
            #print "--------------"
            w.write(output[0] + " (" + output[3] + ", " + output[2] + ") " + output[1])
            w.write('\n')
        except:
            #print str(output)
            #print "2 --------------"
            w.write(str(output))
            w.write('\n')
        #raise Exception("Debug called this")

def save_arm(buff):
                output=[]
                for line in buff:
                        if "Id: " in line:
                                output.append(line.replace("Id: ", " ").replace(" ", "").replace("\n", ""))
                        if " Name: " in line:
                                output.append(line.replace("Name: ", "*").replace(" ", "").replace("\n", ""))
                        if "Def: " in line:
                                output.append(line.replace("Def: ", "").replace(" ", "").replace("\n", ""))
                        if "EquipLv: " in line:
                                output.append(line.replace(" ", "").replace("\n", "").replace("EquipLv:", "Lvl "))

                try:
                        #a.write(output[0] + " (" + output[3] + ", " + output[2] + ") " + output[1])
                        a.write(output[0] + "\t" + output[3] + ", " + output[2] + "\t" + output[1])
                        a.write('\n')
                except:
                        a.write(str(output))
                        a.write('\n')

def save_elm(buff):
                output=[]
                for line in buff:
                        if "Id: " in line:
                                output.append(line.replace("Id: ", " ").replace(" ", "").replace("\n", ""))
                        if " Name: " in line:
                                output.append(line.replace("Name: ", "*").replace(" ", "").replace("\n", ""))
                        if "Def: " in line:
                                output.append(line.replace("Def: ", "").replace(" ", "").replace("\n", ""))
                        if "EquipLv: " in line:
                                output.append(line.replace(" ", "").replace("\n", "").replace("EquipLv:", "Lvl "))

                try:
                        #a.write(output[0] + " (" + output[3] + ", " + output[2] + ") " + output[1])
                        e.write(output[0] + "\t" + output[3] + ", " + output[2] + "\t" + output[1])
                        e.write('\n')
                except:
                        e.write(str(output))
                        e.write('\n')



def save_item(BATATA):
    did=False
    for saved in BATATA:
        if "Loc: 2" in saved and (not "256" in saved):
            did=True
            #print "Saving weapon"
            save_wpn(BATATA)
            break
        elif "Loc: 256" in saved:
            did=True
            #print "Saving armor"
            save_elm(BATATA)
            break
        elif "Loc: 512" in saved:
            did=True
            #print "Saving armor"
            save_arm(BATATA)
            break
    if not did:
            print "Saving unknow item"
            save_wpn(BATATA)

for line in f:
    buff.append(line.replace("\n", ''))
    if '},' in line:
        #buff=[]
        if save:
            save_item(buff)
            save=False
        buff=[]
        continue
    if ("Loc: 2\n" in line) or ("Loc: 512\n" in line)or ("Loc: 256\n" in line):
        save=True


