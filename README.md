# The Mana World: Arena Edition
## Also known as TMW-Æ

In a parallel world, on The Mana World Brazil Universe, just north of the Fortress Island City, is a desert.<br>
This desert is not only dust, though. In the very centre, is **The Champions Arena**.<br>
<br>
You've heard this name a few times. Only the best of the best warriors can survive there.<br>
There is also The Champion, who all aims to win a duel against. The reward is not-humble: 990.000.000 GP.<br>
<br>
<br>
You might not be the best warrior of Froz or Halicarnazo, but you're willing to try it.<br>
How many times some other player is going to knock you down before reaching your objective?<br>
How many challenges will it take to you become The Champion?<br>
And after becoming The Champion, will you be able to complete The Ultimate Quest?<br>
<br>
You won't discover the answers unless you try.<br>

\*\*\*\*

**The Mana World: Arena Edition** (aka. TMW-Æ) is a PK MMORPG.<br>
There are no quests, and all equips must be bought with ingame currency.<br>
There are five arenas:
 * Arena Quirino Voraz: Former “Hungry Quirino Arena”, you cannot bring anything to or from the arena with you.
 * Boss Arena: Form your party and use the Boss Arena with some gold to schedule rapid-leveling up schemes.
 * Scheduled Arena: Every hour there is a new match, going from easy to hard monsters. Will you be able to win it?
 * PvP Arena: You can PK almost everywhere, but this place has just a charm, and a particular smell of blood.
 * The Champions Arena: A terrible map, so your party can challenge the Champion. Finding and killing will be hard with so many enemies.

<br>
<br>

----
*The Mana World: Arena Edition* is part of *The Mana World Brazil* project.<br>
<br>
Staff:<br>
TMW-BR Leader Developer: Jesusalva<br>
TMW-Æ Leader Developer: Jesusalva<br>

Concept: Diogo RBG<br>
<br>
Brought to you by: Andrei Karas (4144)<br>
Brought to you by: Scall Bhramir IV (Scall)<br>
