    Script C: Complete
    Nurse: Complete
    Controlled Warp: Complete
    
    Shopkeeper: Incomplete
    
    MAPCACHE: Complete
    000.tmx: Complete
    
    City.tmx: Complete ← Extra tile of trees on Fringe (instead Top)
    Arena Area: Complete ← Two Passages to Seal
    
    arena.tmx: Complete
    boss.tmx: Complete
    quirino.tmx: Missing
    Candor.tmx: Missing
    Champion.tmx: Complete (Dark Florest)

    Cleanup: Maps
    Integration: Interwined

 ToDo:
    * Only Champion may go to Kitchen and Throne Room
    * NPCs must recognize and respect The Champion
    * OnKill, give points to control discounts and rare-wpn/arm buy
    * Monsters should be correctly scattered
    * Make the 008-2 a visitable map at level 20 (minimum)
    * Attach music files to maps

This is The Mana World: Arena Editon
v1.0
